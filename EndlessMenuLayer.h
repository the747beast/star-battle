//
//  EndlessMenuLayer.h
//  Star Battle
//
//  Created by Jeffrey Barratt on 8/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CCLayer.h"

@interface EndlessMenuLayer : CCLayer
+(id) scene;
@end
