//
//  EndlessMenuLayer.m
//  Star Battle
//
//  Created by Jeffrey Barratt on 8/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EndlessMenuLayer.h"
#import "MainMenuLayer.h"

@implementation EndlessMenuLayer

+(id) scene
{
    CCScene *scene = [CCScene node];
    EndlessMenuLayer *layer = [EndlessMenuLayer node];
    [scene addChild:layer];
    return scene;
}

-(id) init
{
    self = [super init];
    
    CCSprite *menu = [CCSprite spriteWithFile:@"EndlessMenu.png"];
    menu.position = CGPointMake(240, 160);
    [self addChild:menu];
    
    [self schedule:@selector(nextFrame:)];
    
    return self;
}

- (void) nextFrame:(ccTime)dt 
{
    KKInput* input = [KKInput sharedInput];
    CGPoint pos = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
    if (pos.x != 0 && pos.y != 0)
    {
        int x = pos.x;
        int y = pos.y;
        if (x >= 293 && x < 398 && y <= 156 && y >= 51)
        {
            [[CCDirector sharedDirector] replaceScene:
             [CCTransitionFade transitionWithDuration:1.0f scene:[MainMenuLayer scene]]];
        }
        else if (x >= 83 && x < 270 && y <= 320 - 191 && y >= 320 - 261)
        {
            //Defend
        }
        else if (x >= 170 && x < 381 && y <= 320 - 71 && y >= 320 - 137)
        {
            //Attack
        }
    }
}

@end
