//
//  DefendLayer.h
//  Star Battle
//
//  Created by Jeffrey Barratt on 6/13/12.
//  Copyright (c) 2012 Make Games With Us. All rights reserved.
//

#import "CCLayer.h"
#import "ShipAI.h"
#import "MainMenuLayer.h"

@interface DefendLayer : CCLayer
{
    CCSprite *explosion;
    CCAction *explode;
    NSMutableArray *explodeFrames;
}
+(id) scene;
+(id) getAttackers;
-(void) addTo: (CCSprite*) sprite: (int) z;
+(DefendLayer*) getLayer;
+(NSMutableArray*) getShips;
-(void) resetSelected: (ShipAI*) ship;
-(void) addToShots: (CCSprite *) shot;
-(void) stop;

@property (nonatomic, retain) CCSprite *explosion;
@property (nonatomic, retain) CCAction *explode;
@end
