//
//  ShipAI.m
//  Star Battle
//
//  Created by Jeffrey Barratt on 7/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ShipAI.h"
#import "DefendLayer.h"
#import "AttackLayer.h"

@interface ShipAI (PrivateMethods)
-(id) initWithShipImage;
-(void) select;
@end

@implementation ShipAI

@synthesize explosion;
@synthesize explode;

+(id) start: (NSString *) fileName: (BOOL) draw
{
    id ship = [[self alloc] initWithShipImage:fileName];
#ifndef KK_ARC_ENABLED
	[ship autorelease];
#endif // KK_ARC_ENABLED
	return ship;
}

-(id) initWithShipImage: (NSString *) fileName
{
	if ((self = [super initWithFile:fileName]))
	{
		target = CGPointMake(300, 160);
        selected = NO;
        started = NO;
        onTarget = NO;
        health = 150;
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Explosion.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"Explosion.png"];
        [self addChild:spriteSheet];
        explodeFrames = [NSMutableArray array];
        count = 0;
        for(int i = 1; i <= 25; i ++) 
        {
            [explodeFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"explosion_%d.png", i + 10]]];
        }
	}
	return self;
}

-(void) canShoot: (BOOL) shoot
{
    canShoot = shoot;
    if (shoot)
    {
        health = 200;
    }
    else 
    {
        health = 150;
    }
}
-(void) setHealth: (int) i
{
    health = i;
}

-(void) switchTarget: (CGPoint) newTarget
{
    onTarget = false;
    target = newTarget;
    useShip = NO;
}

-(void) targetIs: (CCSprite*) ship
{
    targetShip = ship;
    useShip = YES;
    onTarget = YES;
}

-(void) toggleSelected
{
    selected = !selected;
    
    if(selected)
    {
        sprite = [CCSprite spriteWithFile:@"Selected.png"];
        sprite.position = CGPointMake(22, 21);
        [self addChild:sprite z:-1];
    }
    else 
    {
        [self removeChild:sprite cleanup:YES];
    }
}

-(void) shoot: (CCSprite *) attack
{
    if (canShoot)
    {
        int y = attack.position.y;
        int x = attack.position.x;
        CCSprite *ship = self;
        int shipY = ship.position.y;
        CCSprite *laser = [CCSprite spriteWithFile:@"LaserRed.png"];
        laser.scale = .3f;
        laser.position = CGPointMake(ship.position.x, shipY);
        if (y > shipY)
        {
            laser.rotation = atan((1.0 * (ship.position.x - x)) / (shipY - y)) * 180 / M_PI - 90;
        }
        else 
        {
            laser.rotation = 90 + atan((1.0 * (ship.position.x - x)) / (shipY - y)) * 180 / M_PI;
        }
        [[AttackLayer getLayer] addToShots:laser];
    }
    
}

-(void) refresh
{
    if ([self isWithin:10:target:self.position])
    {
        onTarget = true;
    }
    if (onTarget)
    {
        [self runAI];
    }
    NSMutableArray *ships = [DefendLayer getAttackers];
    if (!canShoot)
    {
        for (int i = 0; i < [ships count]; i ++)
        {
            CCSprite *ship = [ships objectAtIndex:i];
            if (self.position.x > ship.position.x - 26 && self.position.x < ship.position.x + 25 && self.position.y > ship.position.y - 12.5f && self.position.y < ship.position.y + 12.5f)
            {
                [[DefendLayer getLayer] resetSelected:self];
                [self removeChild:sprite cleanup:YES];
                [ships removeObjectAtIndex: i];
                CCAnimation *exploding = [CCAnimation animationWithFrames: explodeFrames delay:0.04f];
                self.explode = [CCAnimate actionWithAnimation:exploding restoreOriginalFrame:NO];
                [self runAction:explode];
                health = 0;
                [[DefendLayer getShips] removeObject:self];
                [[DefendLayer getLayer] stop];
            }
            
        }
    }
    else
    {
        count ++;
        if (count == 30)
        {
            count = 0;
            CCSprite *ship = [self getNearest];
            [self fire:ship.position.x :ship.position.y];
        }
    }
}

-(void) fire: (int) x: (int) y
{
    CCSprite *ship = self;
    int shipY = ship.position.y;
    CCSprite *laser = [CCSprite spriteWithFile:@"Laser.png"];
    laser.scale = .5f;
    laser.position = CGPointMake(ship.position.x, shipY);
    if (y > shipY)
    {
        laser.rotation = atan((1.0 * (ship.position.x - x)) / (shipY - y)) * 180 / M_PI - 90;
    }
    else 
    {
        laser.rotation = 90 + atan((1.0 * (ship.position.x - x)) / (shipY - y)) * 180 / M_PI;
    }
    [[DefendLayer getLayer] addToShots: laser];
}

-(void) runAI
{
    if (!canShoot)
    {
        if (useShip)
        {
            target = targetShip.position;
        }
        else 
        {
            target = [self getNearest].position;
        }
        ShipAI *shot = self;
        float angle = atan((1.0 * (shot.position.x - [shot getTarget].x)) / (shot.position.y - [shot getTarget].y)) * 180 / M_PI;
        if (shot.position.y < [shot getTarget].y)
        {
            shot.rotation += (angle - shot.rotation + 90) / 7;
        }
        else
        {
            shot.rotation += (angle - shot.rotation - 90) / 7;
        }
    }
    else 
    {
        self.rotation += 3;
        if (self.rotation == 360)
        {
            self.rotation = 0;
        }
    }
}

-(CCSprite*) getNearest
{
    NSMutableArray *ships = [DefendLayer getAttackers];
    int index = 0;
    CCSprite *ship = [ships objectAtIndex:index];
    int a = ship.position.x - self.position.x;
    int b = ship.position.y - self.position.y;
    double c = sqrt(1.0 * (a * a + b * b));
    double low = c;
    for (int i = 1; i < [ships count]; i ++)
    {
        ship = [ships objectAtIndex:i];
        a = ship.position.x - self.position.x;
        b = ship.position.y - self.position.y;
        c = sqrt(1.0 * (a * a + b * b));
        if (c < low)
        {
            low = c;
            index = i;
        }
    }
    return [ships objectAtIndex:index];
}

-(CGPoint) getTarget
{
    return target;
}

-(BOOL) isWithin: (int) radius: (CGPoint) pos: (CGPoint) ship
{
    return ship.x > pos.x - radius && ship.x < pos.x + radius && ship.y > pos.y - radius && ship.y < pos.y + radius;
}

-(void) draw {
    [super draw];
    glEnable(GL_LINE_SMOOTH);
    glColor4ub(0, 255, 0, 255);
    glLineWidth(2);
    int x = health / 5;
    CGPoint verts[] = { ccp(20 - x / 2, 50), ccp(20 + x / 2, 50) };
    ccDrawLine(verts[0], verts[1]);
}

-(void) loseHealth: (int) loss
{
    health -= loss;
    if (health <= 0)
    {
        health = 0;
        [[AttackLayer getLayer] destroyMe:self];
    }
}

-(void) healthLoss: (int) loss
{
    health -= loss;
}

-(int) getHealth
{
    return health;
}

@end
