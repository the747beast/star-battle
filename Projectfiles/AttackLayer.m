//
//  AttackLayer.m
//  Star Battle
//
//  Created by Jeffrey Barratt on 6/13/12.
//  Copyright (c) 2012 Make Games With Us. All rights reserved.
//

#import "AttackLayer.h"
#import "ShipAI.h"

@interface AttackLayer (PrivateMethods)
@end

@implementation AttackLayer

bool started = NO;
float x = 240.0f;
int health = 300;
float shipY = 160;
float speed = 5.0f;
int count = 0;
int step = 0;
CCSprite *medShip;
CCSprite *tapToStart;
CCSprite *background;
CCSprite *backgroundCopy;
NSMutableArray *attackers;
NSMutableArray *shots;
NSMutableArray *shotsAttack;
AttackLayer *thisLayer;

@synthesize explosion;
@synthesize explode;

+(id) scene
{
    CCScene *scene = [CCScene node];
    AttackLayer *layer = [AttackLayer node];
    [scene addChild:layer];
    return scene;
}

+(AttackLayer*) getLayer
{
    return thisLayer;
}

-(void) draw
{
    glEnable(GL_LINE_SMOOTH);
    glColor4ub(255, 0, 0, 255);
    glLineWidth(25.0f);
    CGPoint a = CGPointMake(180, 25);
    CGPoint b = CGPointMake(health + 180, 25);
    ccDrawLine(a, b);
}

-(id) init
{
	if ((self = [super init]))
	{
        tapToStart = [CCSprite spriteWithFile:@"Tap_Anywhere.png"];
        tapToStart.position = CGPointMake(240, 160);
        [self addChild:tapToStart z:5];
        attackers = [[NSMutableArray alloc] init];
        shots = [[NSMutableArray alloc] init];
        shotsAttack = [[NSMutableArray alloc] init];
        [self loadLevel];
        thisLayer = self;
        
        [self schedule:@selector(nextFrame:)];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Explosion.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"Explosion.png"];
        [self addChild:spriteSheet];
        explodeFrames = [NSMutableArray array];
        for(int i = 1; i <= 25; i ++) 
        {
            [explodeFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"explosion_%d.png", i + 10]]];
        }
        
	}
    
	return self;
}

- (void) clearLevel
{
    [self removeChild:background cleanup:YES];
    [self removeChild:backgroundCopy cleanup:YES];
    [self removeChild:medShip cleanup:YES];
}

- (void) loadLevel
{
    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentALevel"];
    
    int levelNum = [currentHighScore intValue];
    background = [CCSprite spriteWithFile:@"Menu_Background.png"];
    background.position = CGPointMake(x, 160);
    [self addChild:background z:-5];
    backgroundCopy = [CCSprite spriteWithFile:@"Menu_Background.png"];
    backgroundCopy.position = CGPointMake(x + 480, 160);
    [self addChild:backgroundCopy z:-5];
    
    medShip = [CCSprite spriteWithFile:@"Med_Ship.png"];
    medShip.scale = 0.75f;
    medShip.position = CGPointMake(80, shipY);
    [self addChild:medShip z:-3];
    
    NSDictionary *level = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"level%i.plist", levelNum]];
    NSArray *enemies = [level objectForKey:@"enemies"];
    for (int i = 0; i < [enemies count]; i ++)
    {
        NSDictionary *enemy = [enemies objectAtIndex:i];
        ShipAI *sprite = [ShipAI start:[enemy valueForKey:@"name"]:NO];
        sprite.position = CGPointMake([[enemy valueForKey:@"x"] intValue] + 480, [[enemy valueForKey:@"y"] intValue]);
        [attackers addObject:sprite];
        [self addChild:sprite z:-5];
    }
}

- (void) nextFrame:(ccTime)dt 
{
    
    KKInput* input = [KKInput sharedInput];
    CGPoint pos = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
    if (started)
    {
        x -= .5;
        if (x == -240)
        {
            x = 240;
        }
        background.position = CGPointMake(x, 160);
        backgroundCopy.position = CGPointMake(x + 480, 160);
        for (int i = 0; i < [attackers count]; i ++)
        {
            NSInteger first = i;
            CCSprite *attacker = [attackers objectAtIndex:first];
            int x = attacker.position.x;
            attacker.position = CGPointMake(x - 0.7f, attacker.position.y);
        }
        
        for (int i = 0; i < [shots count]; i ++)
        {
            CCSprite *shot = [shots objectAtIndex:i];
            shot.position = CGPointMake(speed * cos((shot.rotation) / 180 * M_PI) + shot.position.x, -speed * sin((shot.rotation) / 180 * M_PI) + shot.position.y);
        }
        for (int i = 0; i < [shotsAttack count]; i ++)
        {
            CCSprite *shot = [shotsAttack objectAtIndex:i];
            shot.position = CGPointMake(speed * cos((shot.rotation) / 180 * M_PI) + shot.position.x, -speed * sin((shot.rotation) / 180 * M_PI) + shot.position.y);
        }
        
        [self checkHit];
        [self mainHit];
        
        if (pos.x != 0 && pos.y != 0 && count > 15)
        {
            if (pos.x > 155)
            {
                [self fire:pos.x:pos.y];
                count = 0;
            }
            else 
            {
                if (pos.y > shipY)
                {
                    shipY += 2;
                }
                else 
                {
                    shipY -= 2;
                }
                medShip.position = CGPointMake(80, shipY);
            }
        }
        count ++;
        step ++;
        if (step == 100)
        {
            for (int i = 0; i < [attackers count]; i ++)
            {
                ShipAI *ship = [attackers objectAtIndex:i];
                [ship shoot:medShip];
            }
            step = 0;
        }
    }
    else 
    {
        if (pos.x != 0 && pos.y != 0)
        {
            started = YES;
            [self removeChild:tapToStart cleanup:YES];
        }
    }
}

-(void) addToShots: (CCSprite*) sprite
{
    [self addChild:sprite];
    [shotsAttack addObject:sprite];
}

-(void) fire: (int) x: (int) y
{
    CCSprite *laser = [CCSprite spriteWithFile:@"Laser.png"];
    laser.position = CGPointMake(145, shipY);
    if (y > shipY)
    {
        laser.rotation = atan((1.0 * (145 - x)) / (shipY - y)) * 180 / M_PI - 90;
    }
    else 
    {
        laser.rotation = 90 + atan((1.0 * (145 - x)) / (shipY - y)) * 180 / M_PI;
    }
    [shots addObject:laser];
    [self addChild:laser z:-4];
}

-(void) fireSmall
{
    int y = medShip.position.y;
    int x = medShip.position.x;
    CCSprite *laser = [CCSprite spriteWithFile:@"LaserRed.png"];
    laser.scale = .5f;
    laser.position = CGPointMake(145, shipY);
    if (y > shipY)
    {
        laser.rotation = atan((1.0 * (145 - x)) / (shipY - y)) * 180 / M_PI - 90;
    }
    else 
    {
        laser.rotation = 90 + atan((1.0 * (145 - x)) / (shipY - y)) * 180 / M_PI;
    }
    [shots addObject:laser];
    [self addChild:laser z:-2];
}

-(void) checkHit
{
    for (int j = 0; j < [attackers count]; j ++)
    {
        ShipAI *ship = [attackers objectAtIndex:j];
        for (int i = 0; i < [shots count]; i ++)
        {
            CCSprite *shot = [shots objectAtIndex:i];
            if (shot.position.x < ship.position.x + 20.0f && shot.position.x > ship.position.x - 20.0f)
            {
                if (shot.position.y < ship.position.y + 20.0f && shot.position.y > ship.position.y - 20.0f)
                {
                    [self removeChild:shot cleanup:YES];
                    [shots removeObjectAtIndex:i];
                    [ship loseHealth:75];
                }
            }
            if (shot.position.x < -20 || shot.position.x > 500 || shot.position.y < -20 || shot.position.y > 380)
            {
                [self removeChild:shot cleanup:YES];
                [shots removeObjectAtIndex:i];
            }
        }
        if (ship.position.x < 145)
        {
            if (ship.position.y > shipY - 40 && ship.position.y < shipY + 40)
            {
                NSLog(@"Health Loss");
            }
            CCAnimation *exploding = [CCAnimation animationWithFrames: explodeFrames delay:0.04f];
            self.explode = [CCAnimate actionWithAnimation:exploding restoreOriginalFrame:NO];
            [ship runAction:explode];
            [attackers removeObjectAtIndex:j];
        }
    }
}

-(void) destroyMe: (ShipAI*) ship
{
    CCAnimation *exploding = [CCAnimation animationWithFrames: explodeFrames delay:0.04f];
    self.explode = [CCAnimate actionWithAnimation:exploding restoreOriginalFrame:NO];
    [ship runAction:explode];
    [attackers removeObject:ship];
}

-(void) mainHit
{
    for (int i = 0; i < [shotsAttack count]; i ++)
    {
        CCSprite *shot = [shotsAttack objectAtIndex:i];
        if (shot.position.x < medShip.position.x + 75.0f && shot.position.x > medShip.position.x - 75.0f)
        {
            if (shot.position.y < medShip.position.y + 40.0f && shot.position.y > medShip.position.y - 40.0f)
            {
                CCAnimation *exploding = [CCAnimation animationWithFrames: explodeFrames delay:0.04f];
                self.explode = [CCAnimate actionWithAnimation:exploding restoreOriginalFrame:NO];
                [shot runAction:explode];
                [shotsAttack removeObjectAtIndex:i];
                health -= 50;
            }
        }
    }
}

@end