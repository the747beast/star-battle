//
//  ShipAI.h
//  Star Battle
//
//  Created by Jeffrey Barratt on 7/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CCSprite.h"

@interface ShipAI : CCSprite
{
    NSMutableArray *attackers;
    BOOL selected;
    CCSprite *sprite;
    BOOL onTarget;
    CGPoint target;
    CCSprite *targetShip;
    BOOL started;
    BOOL useShip;
    CCSprite *explosion;
    CCAction *explode;
    NSMutableArray *explodeFrames;
    int health;
    bool canShoot;
    int count;
}
-(void) toggleSelected;
+(id) start: (NSString *) fileName: (bool) draw;
-(void) switchTarget: (CGPoint) newTarget;
-(void) refresh;
-(CGPoint) getTarget;
-(void) targetIs: (CCSprite*) ship;
-(void) canShoot: (BOOL) shoot;
-(void) shoot: (CCSprite *) attacker;
-(void) loseHealth: (int) loss;
-(void) healthLoss: (int) loss;
-(int) getHealth;
-(void) setHealth: (int) i;

@property (nonatomic, retain) CCSprite *explosion;
@property (nonatomic, retain) CCAction *explode;

@end
