//
//  DefendLayer.m
//  Star Battle
//
//  Created by Jeffrey Barratt on 6/13/12.
//  Copyright (c) 2012 Make Games With Us. All rights reserved.
//

#import "DefendLayer.h"

@interface DefendLayer (PrivateMethods)
-(void) addChild:(CCNode *)node: (int) z;
@end

@implementation DefendLayer

@synthesize explosion;
@synthesize explode;

CCSprite *crosshair;
CCSprite *menu;
NSMutableArray *shots;
NSMutableArray *myShots;
NSMutableArray *attackers;
NSMutableArray *leavers;
NSMutableArray *attackersPoint;
NSMutableArray *ships;
float shotSpeed = 5;
float shipSpeed = .9f;
float bigShipSpeed = .1f;
BOOL clicked;
BOOL attacking = NO;
BOOL top = YES;
BOOL onShip = NO;
BOOL stop;
int currentSelected;
int set = 0;
int wave = 0;

DefendLayer* thisLayer;

+(id) scene
{
    CCScene *scene = [CCScene node];
    DefendLayer *layer = [DefendLayer node];
    [scene addChild:layer];
    return scene;
}

+(DefendLayer*) getLayer
{
    return thisLayer;
}

-(id) init
{
	if ((self = [super init]))
	{
        currentSelected = -1;
        top = YES;
        thisLayer = self;
        clicked = NO;
        stop = NO;
        menu = [CCSprite spriteWithFile:@"Pause.png"];
        menu.position = CGPointMake(240, 160);
        attackers = [[NSMutableArray alloc] init];
        leavers = [[NSMutableArray alloc] init];
        attackersPoint = [[NSMutableArray alloc] init];
        ships = [[NSMutableArray alloc] init];
        shots = [[NSMutableArray alloc] init];
        myShots = [[NSMutableArray alloc] init];
        [self loadLevel];
        [self schedule:@selector(nextFrame:)];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Explosion.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"Explosion.png"];
        [self addChild:spriteSheet];
        explodeFrames = [NSMutableArray array];
        for(int i = 1; i <= 25; i ++) 
        {
            [explodeFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"explosion_%d.png", i + 10]]];
        }
	}
    
	return self;
}

-(void) loadLevel
{
    wave ++;
    if (wave == 1)
    {
        CCSprite *background = [CCSprite spriteWithFile:@"Menu_Background.png"];
        background.position = CGPointMake(240, 160);
        [self addChild:background z:-6];
        
        CCSprite *largeShip = [CCSprite spriteWithFile:@"Large_Ship.png"];
        largeShip.scale = 0.5f;
        largeShip.position = CGPointMake(400, 160);
        [self addChild:largeShip z:-3];
        
        CCSprite *menu = [CCSprite spriteWithFile:@"Toolbar.png"];
        menu.scale = .5;
        menu.position = CGPointMake(240, 20);
        [self addChild:menu z:-5];
        
        CCSprite *menuItem = [CCSprite spriteWithFile:@"Small_Ship.png"];
        menuItem.scale = .5;
        menuItem.position = CGPointMake(340, 20);
        [self addChild:menuItem];
        menuItem = [CCSprite spriteWithFile:@"Shooter_Ship.png"];
        menuItem.scale = .5;
        menuItem.position = CGPointMake(380, 22);
        [self addChild:menuItem];
        //    menuItem = [CCSprite spriteWithFile:@"Small_Ship.png"];
        //    menuItem.scale = .5;
        //    menuItem.position = CGPointMake(420, 20);
        //    [self addChild:menuItem];
        //    menuItem = [CCSprite spriteWithFile:@"Small_Ship.png"];
        //    menuItem.scale = .5;
        //    menuItem.position = CGPointMake(460, 20);
        //    [self addChild:menuItem];
    }
    
    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentDLevel"];
    int levelNum = [currentHighScore intValue];
    NSDictionary *level = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"level_%i.plist", levelNum]];
    NSArray *enemies = [level objectForKey:@"enemies"];
    for (int i = 0; i < [enemies count]; i ++)
    {
        NSDictionary *enemy = [enemies objectAtIndex:i];
        ShipAI *sprite = [ShipAI start:[enemy valueForKey:@"name"]:NO];
        [sprite setHealth:300];
        sprite.position = CGPointMake(-[[enemy valueForKey:@"x"] intValue], [[enemy valueForKey:@"y"] intValue]);
        sprite.scale = .25f;
        [ships addObject:sprite];
        [self addChild:sprite z:-4];
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
-(void) addShip: (NSString*) shipName
{
    if (set > 10)
    {
        ShipAI *smallShip = [ShipAI start:shipName: true];
        smallShip.scale = 0.5f;
        if (top)
        {
            smallShip.position = CGPointMake(400, 210);
            smallShip.rotation = 90;
            top = NO;
        }
        else 
        {
            smallShip.position = CGPointMake(400, 110);
            smallShip.rotation = -90;
            top = YES;
        }
        if (shipName == @"Small_Ship.png")
        {
            [smallShip canShoot:NO];
        }
        else if (shipName == @"Shooter_Ship.png")
        {
            [smallShip canShoot:YES];
        }
        [leavers addObject:smallShip];
        [self addChild:smallShip z:-4];
        set = -10;
    }
}

- (void) nextFrame:(ccTime)dt 
{
    if (!stop)
    {
        
        KKInput* input = [KKInput sharedInput];
        CGPoint pos = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
        
        
        
        if (pos.x != 0 && pos.y != 0)
        {
            if (pos.y > 40)
            {
                BOOL select = false;
                bool inLoop = NO;
                for (int i = 0; i < [attackers count]; i ++)
                {
                    ShipAI *ship = [attackers objectAtIndex:i];
                    if (pos.x >= ship.position.x - 12.5 && pos.x <= ship.position.x + 12.5 && pos.y >= ship.position.y - 12.5 && pos.y <= ship.position.y + 12.5)
                    {
                        if (currentSelected != -1)
                        {
                            ShipAI *s = [attackers objectAtIndex:currentSelected];
                            [s toggleSelected];
                        }
                        ShipAI *attacker = [attackers objectAtIndex:i];
                        crosshair.position = [attacker getTarget];
                        CGPoint tempPos = crosshair.position;
                        for (int i = 0; i < [ships count]; i ++)
                        {
                            CCSprite *ship = [ships objectAtIndex:i];
                            if (crosshair.position.x > ship.position.x - 25 && crosshair.position.x < ship.position.x + 25 && crosshair.position.y > ship.position.y - 12.5f && crosshair.position.y < ship.position.y + 12.5f)
                            {
                                [self removeChild:crosshair cleanup:YES];
                                crosshair = [CCSprite spriteWithFile:@"Crosshair.png"];
                                crosshair.position = tempPos;
                                [self addChild:crosshair z:11];
                                inLoop = YES;
                                onShip = YES;
                            }
                        }
                        if (!inLoop)
                        {
                            [self removeChild:crosshair cleanup:YES];
                            crosshair = [CCSprite spriteWithFile:@"Gray_Crosshair.png"];
                            crosshair.position = tempPos;
                            [self addChild:crosshair z:11];
                            onShip = NO;
                        }
                        [attacker toggleSelected];
                        currentSelected = i;
                        select = true;
                    }
                }
                if (!select && currentSelected != -1)
                {
                    ShipAI *attacker = [attackers objectAtIndex:currentSelected];
                    float angle = atan((1.0 * (attacker.position.x - pos.x)) / (attacker.position.y - pos.y)) * 180 / M_PI;
                    if (attacker.position.y < pos.y)
                    {
                        [attacker runAction:[CCRotateBy actionWithDuration:0.5f angle:(angle + 90) - attacker.rotation]];
                    }
                    else
                    {
                        [attacker runAction:[CCRotateBy actionWithDuration:0.5f angle:(angle - 90) - attacker.rotation]];
                    }
                }
                
                if (currentSelected != -1 && !select)
                {
                    ShipAI *attacker = [attackers objectAtIndex:currentSelected];
                    CGPoint tempPos = crosshair.position;
                    int index = 0;
                    inLoop = NO;
                    for (int i = 0; i < [ships count]; i ++)
                    {
                        CCSprite *ship = [ships objectAtIndex:i];
                        if (pos.x > ship.position.x - 25 && pos.x < ship.position.x + 25 && pos.y > ship.position.y - 12.5f && pos.y < ship.position.y + 12.5f && !select)
                        {
                            [self removeChild:crosshair cleanup:YES];
                            crosshair = [CCSprite spriteWithFile:@"Crosshair.png"];
                            crosshair.position = tempPos;
                            [self addChild:crosshair z:11];
                            [attacker targetIs:ship];
                            inLoop = YES;
                            onShip = YES;
                            index = i;
                        }
                    }
                    if (!inLoop)
                    {
                        [self removeChild:crosshair cleanup:YES];
                        crosshair = [CCSprite spriteWithFile:@"Gray_Crosshair.png"];
                        crosshair.position = tempPos;
                        [self addChild:crosshair z:11];
                        onShip = NO;
                    }
                    if(!select)
                    {
                        [crosshair runAction:[CCMoveTo actionWithDuration:.5 position:pos]];
                        [attacker switchTarget:pos];
                    }
                    if (inLoop)
                    {
                        CCSprite *ship = [ships objectAtIndex:index];
                        [attacker targetIs:ship];
                    }
                }
            }
            else 
            {
                if (pos.x < 40)
                {
                    stop = true;
                    [self setPause];
                }
                else if(pos.x < 193)
                {
                    if (currentSelected != -1)
                    {
                        ShipAI *ship = [attackers objectAtIndex:currentSelected];
                        [ship toggleSelected];
                        currentSelected = -1;
                        [self removeChild:crosshair cleanup:YES];
                    }
                }
                else if (pos.x > 322 && pos.x < 361)
                {
                    [self addShip: @"Small_Ship.png"];
                }
                else if (pos.x > 361 && pos.x < 400)
                {
                    [self addShip: @"Shooter_Ship.png"];
                }
                else if (pos.x >= 400 && pos.x < 440)
                {
                    
                }
                else if (pos.x >= 440)
                {
                    
                }
            }
        }
        
        
        
        for (int i = 0; i < [attackers count]; i ++)
        {
            ShipAI *shot = [attackers objectAtIndex:i];
            shot.position = CGPointMake(- shipSpeed * cos((shot.rotation) / 180 * M_PI) + shot.position.x, + shipSpeed * sin((shot.rotation) / 180 * M_PI) + shot.position.y);
        }
        for (int i = 0; i < [leavers count]; i ++)
        {
            ShipAI *shot = [leavers objectAtIndex:i];
            if (shot.position.y > 160)
            {
                shot.position = CGPointMake(shot.position.x, shot.position.y + shipSpeed);
            }
            else 
            {
                shot.position = CGPointMake(shot.position.x, shot.position.y - shipSpeed);
            }
            if (shot.position.y > 240 || shot.position.y < 80)
            {
                float angle = atan((1.0 * (shot.position.x - [shot getTarget].x)) / (shot.position.y - [shot getTarget].y)) * 180 / M_PI;
                if (shot.position.y < [shot getTarget].y)
                {
                    [shot runAction:[CCRotateBy actionWithDuration:0.5f angle:(angle + 90) - shot.rotation]];
                }
                else
                {
                    [shot runAction:[CCRotateBy actionWithDuration:0.5f angle:(angle - 90) - shot.rotation]];
                }
                [leavers removeObject:shot];
                [attackers addObject:shot];
            }
        }
        for (int i = 0; i < [ships count]; i ++)
        {
            CCSprite *ship = [ships objectAtIndex:i];
            ship.position = CGPointMake(ship.position.x + bigShipSpeed, ship.position.y);
        }
        for (int i = 0; i < [ships count]; i ++)
        {
            CCSprite *ship = [ships objectAtIndex:i];
            if (crosshair.position.x > ship.position.x - 26 && crosshair.position.x < ship.position.x + 25 && crosshair.position.y > ship.position.y - 12.5f && crosshair.position.y < ship.position.y + 12.5f && onShip)
            {
                crosshair.position = ship.position;
            }
        }
        set ++;
        if (set == 2000)
        {
            set = 11;
        }
        if (set % 100 == 0)
        {
            if ([attackers count] > 0)
            {
                for (int i = 0; i < [ships count]; i ++)
                {
                    [self findNearest:i];
                }
            }
        }
        for (int i = 0; i < [shots count]; i ++)
        {
            CCSprite *shot = [shots objectAtIndex:i];
            shot.position = CGPointMake(shotSpeed * cos((shot.rotation) / 180 * M_PI) + shot.position.x, -shotSpeed * sin((shot.rotation) / 180 * M_PI) + shot.position.y);
        }
        for (int i = 0; i < [myShots count]; i ++)
        {
            CCSprite *shot = [myShots objectAtIndex:i];
            shot.position = CGPointMake(shotSpeed * cos((shot.rotation) / 180 * M_PI) + shot.position.x, -shotSpeed * sin((shot.rotation) / 180 * M_PI) + shot.position.y);
        }
        
        [self checkHit];
        
        //Now run all ship's update method
        for (int i = 0; i < [attackers count]; i ++)
        {
            ShipAI *ship = [attackers objectAtIndex:i];
            [ship refresh];
        }
    }
}

-(void) setPause
{
    [self addChild:menu z:99];
    CCMenuItemImage * menuItem1 = [CCMenuItemImage itemFromNormalImage:@"Pause_Menu1.png"
                                                         selectedImage:@"Pause_Menu1_Selected.png"
                                                                target:self
                                                              selector:@selector(goMenu)];
    CCMenuItemImage * menuItem2 = [CCMenuItemImage itemFromNormalImage:@"Pause_Menu2.png"
                                                         selectedImage:@"Pause_Menu2_Selected.png"
                                                                target:self
                                                              selector:@selector(shop)];
    CCMenuItemImage * menuItem3 = [CCMenuItemImage itemFromNormalImage:@"Pause_Menu3.png"
                                                         selectedImage:@"Pause_Menu3_Selected.png"
                                                                target:self
                                                              selector:@selector(resume)];
    CCMenu * myMenu = [CCMenu menuWithItems: menuItem1, menuItem2, menuItem3, nil];
    myMenu.position = CGPointMake(240, 120);
    [myMenu alignItemsVerticallyWithPadding:15.0f];
    [self addChild:myMenu z:100 tag:1];
}

-(void) resume
{
    stop = NO;
    [self removeChild:menu cleanup:YES];
    [self removeChildByTag:1 cleanup:YES];
}

-(void) shop
{
    
}

-(void) goMenu
{
    [[CCDirector sharedDirector] replaceScene:
	 [CCTransitionFade transitionWithDuration:1.0f scene:[MainMenuLayer scene]]];
}

+(NSMutableArray*) getAttackers
{
    return ships;
}

+(NSMutableArray*) getShips
{
    return attackers;
}

-(void) findNearest: (int) shipNum
{
    CCSprite *ship = [ships objectAtIndex:shipNum];
    int index = 0;
    ShipAI *attacker = [attackers objectAtIndex:0];
    int low = [self getDistance:attacker :ship];
    for (int i = 1; i < [attackers count]; i ++)
    {
        attacker = [attackers objectAtIndex:i];
        int distance = [self getDistance:attacker :ship];
        if (distance < low)
        {
            low = distance;
            index = i;
        }
    }
    attacker = [attackers objectAtIndex:index];
    [self fireBig:attacker.position.x:attacker.position.y :shipNum];
}

-(int) getDistance: (ShipAI*) attacker: (CCSprite*) ship
{
    int a = (attacker.position.x - ship.position.x);
    int b = (attacker.position.y - ship.position.y);
    double hyp = sqrt(1.0 * (a * a + b * b));
    return hyp;
}

-(void) fireBig: (int) x: (int) y: (int) shipNum
{
    CCSprite *ship = [ships objectAtIndex:shipNum];
    int shipY = ship.position.y;
    CCSprite *laser = [CCSprite spriteWithFile:@"LaserRed.png"];
    laser.scale = .5f;
    laser.position = CGPointMake(ship.position.x + 25, shipY);
    if (y > shipY)
    {
        laser.rotation = atan((1.0 * (ship.position.x + 25 - x)) / (shipY - y)) * 180 / M_PI - 90;
    }
    else 
    {
        laser.rotation = 90 + atan((1.0 * (ship.position.x + 25 - x)) / (shipY - y)) * 180 / M_PI;
    }
    [shots addObject:laser];
    [self addChild:laser z:-4];
}

-(void) addToShots: (CCSprite *) shot
{
    [myShots addObject:shot];
    [self addChild:shot z:-3];
}

-(void) draw
{
    [super draw];
    glEnable(GL_LINE_SMOOTH);
    glColor4ub(255, 0, 0, 255);
    glLineWidth(3);
    for (int i = 0; i < [ships count]; i ++)
    {
        ShipAI *ship = [ships objectAtIndex:i];
        int health = [ship getHealth];
        CGPoint a = CGPointMake(ship.position.x - health / 10, ship.position.y + 25);
        CGPoint b = CGPointMake(ship.position.x + health / 10, ship.position.y + 25);
        ccDrawLine(a, b);
    }
}

-(void) resetSelected: (ShipAI*) ship
{
    int i = [attackers indexOfObject:ship];
    if (i == currentSelected)
    {
        currentSelected = -1;
        [self removeChild:crosshair cleanup:YES];
    }
    else if (i < currentSelected)
    {
        currentSelected --;
    }
}

-(void) checkHit
{
    for (int i = 0; i < [ships count]; i ++)
    {
        ShipAI *ship = [ships objectAtIndex:i];
        for (int j = 0; j < [myShots count]; j ++)
        {
            CCSprite *shot = [myShots objectAtIndex:j];
            if (shot.position.x > ship.position.x - 26 && shot.position.x < ship.position.x + 25 && shot.position.y > ship.position.y - 12.5f && shot.position.y < ship.position.y + 12.5f)
            {
                [myShots removeObjectAtIndex:j];
                [self removeChild:shot cleanup:YES];
                [ship healthLoss:50];
                if ([ship getHealth] <= 0)
                {
                    [ship setHealth:0];
                    [ships removeObject:ship];
                    CCAnimation *exploding = [CCAnimation animationWithFrames: explodeFrames delay:0.04f];
                    self.explode = [CCAnimate actionWithAnimation:exploding restoreOriginalFrame:NO];
                    [ship runAction:explode];
                    if ([ships count] == 1)
                    {
                        [self stop];
                        return;
                    }
                }
            }
        }
        
    }
    
    for (int i = 0; i < [attackers count]; i ++)
    {
        ShipAI *ship = [attackers objectAtIndex:i];
        for (int j = 0; j < [shots count]; j ++)
        {
            CCSprite *shot = [shots objectAtIndex:j];
            if (shot.position.x > ship.position.x - 7.5f && shot.position.x < ship.position.x + 7.5f && shot.position.y > ship.position.y - 7.5f && shot.position.y < ship.position.y + 7.5f)
            {
                [ship healthLoss:75];
                [self removeChild:shot cleanup:YES];
                [shots removeObject:shot];
                if ([ship getHealth] <= 0)
                {
                    [ship setHealth:0];
                    [attackers removeObject:ship];
                    CCAnimation *exploding = [CCAnimation animationWithFrames: explodeFrames delay:0.04f];
                    self.explode = [CCAnimate actionWithAnimation:exploding restoreOriginalFrame:NO];
                    [ship runAction:explode];
                    
                }
                
            }
        }
        
    }
}

-(void) stop
{
//    stop = true;
    [self loadLevel];
}

@end