//
//  AttackLayer.h
//  Star Battle
//
//  Created by Jeffrey Barratt on 6/13/12.
//  Copyright (c) 2012 Make Games With Us. All rights reserved.
//

#import "CCLayer.h"
#import "ShipAI.h"

@interface AttackLayer : CCLayer
{
    CCSprite *explosion;
    CCAction *explode;
    NSMutableArray *explodeFrames;
}
+(id) scene;
+(AttackLayer*) getLayer;
-(void) addToShots: (CCSprite*) laser;
-(void) destroyMe: (ShipAI*) ship;

@property (nonatomic, retain) CCSprite *explosion;
@property (nonatomic, retain) CCAction *explode;
@end
