//
//  MainMenuLayer.h
//  Star Battle
//
//  Created by Jeffrey Barratt on 6/13/12.
//  Copyright (c) 2012 Make Games With Us. All rights reserved.
//

#import "CCLayer.h"
#import "EndlessMenuLayer.h"

@interface MainMenuLayer : CCLayer
+(id) scene;
@end
