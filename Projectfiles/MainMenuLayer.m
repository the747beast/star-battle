//
//  MainMenuLayer.m
//  Star Battle
//
//  Created by Jeffrey Barratt on 6/13/12.
//  Copyright (c) 2012 Make Games With Us. All rights reserved.
//

#import "MainMenuLayer.h"

@implementation MainMenuLayer

CCSprite *about;
CCSprite *moreGames;
CCSprite *campaign;
bool special;
bool onAbout;
bool onCampaign;

+(id) scene
{
    CCScene *scene = [CCScene node];
    MainMenuLayer *layer = [MainMenuLayer node];
    [scene addChild:layer];
    return scene;
}

-(id) init
{
    if(self = [super init])
    {
        CCSprite *background = [CCSprite spriteWithFile:@"MainMenu.png"];
        background.position = CGPointMake(240, 160);
        [self addChild:background];
        
        about = [CCSprite spriteWithFile:@"AboutMe.png"];
        about.position = CGPointMake(240, 160);
        moreGames = [CCSprite spriteWithFile:@"MoreGames.png"];
        moreGames.position = CGPointMake(240, 160);
        campaign = [CCSprite spriteWithFile:@"CampaignMainMenu.png"];
        campaign.position = CGPointMake(240, 160);
        
        [self schedule:@selector(nextFrame:)];
    }
    return self;
}

- (void) nextFrame:(ccTime)dt 
{
    KKInput* input = [KKInput sharedInput];
    CGPoint pos = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
    if (pos.x != 0 && pos.y != 0)
    {
        if (special)
        {
            special = false;
            if (onAbout)
            {
                [self removeChild:about cleanup:YES];
            }
            else 
            {
                if (onCampaign)
                {
                    [self removeChild:campaign cleanup:YES];
                }
                else
                {
                    [self removeChild:moreGames cleanup:YES];
                }
            }
        }
        int x = pos.x;
        int y = pos.y;
        if (x >= 12 && x < 91 && y <= 320 - 124 && y >= 320 - 198)
        {
            [self addChild:campaign];
            onCampaign = true;
            onAbout = false;
            special = true;
        }
        else if (x >= 91 && x < 171 && y <= 320 - 120 && y >= 320 - 186)
        {
            [[CCDirector sharedDirector] replaceScene:
             [CCTransitionFade transitionWithDuration:1.0f scene:[EndlessMenuLayer scene]]];
        }
        else if (x >= 201 && x < 281 && y <= 320 - 101 && y >= 320 - 169)
        {
            NSLog(@"3");
        }
        else if (x >= 302 && x < 382 && y <= 320 - 119 && y >= 320 - 187)
        {
            [self addChild:about];
            special = true;
            onAbout = true;
        }
        else if (x >= 382 && x < 462 && y <= 320 - 127 && y >= 320 - 195)
        {
            [self addChild:moreGames];
            special = true;
            onAbout = false;
            onCampaign = false;
        }
    }
}

//-(void) startAttackGame
//{
//    [[CCDirector sharedDirector] replaceScene:
//	 [CCTransitionFade transitionWithDuration:1.0f scene:[AttackLayer scene]]];
//    NSNumber *attackLevel = [NSNumber numberWithInteger:1];
//    [[NSUserDefaults standardUserDefaults] setObject:attackLevel forKey:@"currentALevel"];
//}
//
//-(void) startDefendGame
//{
//    [[CCDirector sharedDirector] replaceScene:
//	 [CCTransitionFade transitionWithDuration:1.0f scene:[DefendLayer scene]]];
//    NSNumber *attackLevel = [NSNumber numberWithInteger:1];
//    [[NSUserDefaults standardUserDefaults] setObject:attackLevel forKey:@"currentDLevel"];
//}

@end
